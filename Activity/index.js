// querySelector() method returns the first element that matches a selector

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// addEventListener() - is a method that attaches an event handler to an element
// 'keyup' - enters the key after being released from being pushed down.

txtFirstName.addEventListener('keyup', (event) => { spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;});

/*
	What is the event target value?
	- event.target - gives you the element that triggered the event
	- event.target.value - return the element where the event occured.
*/

txtLastName.addEventListener('keyup', (event) => {spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value});

	// the target property is read-only
	console.log(event.target)
	console.log(event.target.value)